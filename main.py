from os import environ

from yaml import dump
from requests import get


def fetch_tags():
    data = {"ubuntu18.04": [], "ubuntu20.04": []}
    all_tags = get(
        "https://registry.hub.docker.com/v1/repositories/nvidia/cuda/tags"
    ).json()
    for raw_tag in all_tags:
        tag = raw_tag["name"]
        for os in data.keys():
            if (os in tag) and ("cudnn" in tag):
                data[os].append(tag)
                break
    return data


def generate_ci(data):
    ci = {
        "default": {
            "image": {
                "name": "gcr.io/kaniko-project/executor:v1.6.0-debug",
                "entrypoint": [""],
            },
            "before_script": [
                "mkdir -p /kaniko/.docker",
                'echo "{\\"auths\\":{\\"$CI_REGISTRY\\":{\\"username\\":\\"$CI_REGISTRY_USER\\",\\"password\\":\\"$CI_REGISTRY_PASSWORD\\"}}}" > /kaniko/.docker/config.json',
            ],
            "tags": ["gce", "docker"],
        },
        "workflow": {"rules": [{"when": "always"}]},
    }
    # TODO: limited to only few base images for faster dev
    for os, tags in list(data.items())[:1]:
        for tag in tags[:1]:
            for python_version in ("3.7", "3.8", "3.9"):
                ci[f"{tag} || python {python_version}"] = {
                    "script": f"/kaniko/executor --destination=${{CI_REGISTRY_IMAGE}}:{tag}-python{python_version}-{environ['VERSION']} --build-arg IMAGE=nvidia/cuda:{tag} --build-arg PYTHON_VERSION={python_version} --dockerfile=./docker_templates/ubuntu.Dockerfile --cache=true"
                }
    with open(environ["CI_FILE"], "w") as generated_ci:
        dump(ci, generated_ci, default_flow_style=False, width=float("inf"))


if __name__ == "__main__":
    generate_ci(fetch_tags())
