#docker build -t t -f docker_templates/ubuntu.Dockerfile --build-arg IMAGE=nvidia/cuda:9.0-devel-ubuntu16.04 .
ARG IMAGE
FROM ${IMAGE}
ARG PYTHON_VERSION
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && \
    apt-get install software-properties-common curl -y && \
    add-apt-repository ppa:deadsnakes/ppa -y && \
    apt-get update && \
    apt-get install python${PYTHON_VERSION} python${PYTHON_VERSION}-distutils -y && \
    ln -s python${PYTHON_VERSION} /usr/bin/python && \
    curl https://bootstrap.pypa.io/get-pip.py | python
